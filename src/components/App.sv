<script>
  import Game from "./Game";
</script>

<style>
  :global(body) {
    font: 14px "Century Gothic", Futura, sans-serif;
    margin: 20px;
  }
</style>

<Game />
