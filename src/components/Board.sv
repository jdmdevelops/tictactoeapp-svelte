<script>
  import Square from "./Square";

  export let gameBoard;
  export let handleClick;
</script>

<style>
  .board-row:after {
    clear: both;
    content: "";
    display: table;
  }
</style>

<div class="board">
  <div class="board-row">
    <Square {gameBoard} id="0" {handleClick} />
    <Square {gameBoard} id="1" {handleClick} />
    <Square {gameBoard} id="2" {handleClick} />
  </div>
  <div class="board-row">
    <Square {gameBoard} id="3" {handleClick} />
    <Square {gameBoard} id="4" {handleClick} />
    <Square {gameBoard} id="5" {handleClick} />
  </div>
  <div class="board-row">
    <Square {gameBoard} id="6" {handleClick} />
    <Square {gameBoard} id="7" {handleClick} />
    <Square {gameBoard} id="8" {handleClick} />
  </div>
</div>
