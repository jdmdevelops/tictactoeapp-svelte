<script>
  import Board from "./Board";

  let history = [
    {
      gameBoard: Array(9).fill(null)
    }
  ];

  let xIsNext = true;
  let status;
  let winner;
  let stepNumber = 0;

  const handleClick = i => {
    const clone = history.slice(0, stepNumber + 1);
    const current = clone[clone.length - 1];
    const squares = current.gameBoard.slice();
    if (calculateWinner(squares) || squares[i]) return;

    squares[i] = xIsNext ? "X" : "O";
    stepNumber = clone.length;

    history = clone.concat([{ gameBoard: squares }]);
    xIsNext = !xIsNext;

    winner = calculateWinner(squares);
  };
  $: current = history[stepNumber];

  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return squares[a];
      }
    }
    return null;
  }

  const jumpTo = step => {
    stepNumber = step;
    xIsNext = step % 2 === 0;
  };

  $: status = winner
    ? "Winner: " + winner + "!"
    : "Next player: " + (xIsNext ? "X" : "O");
</script>

<style>
  .game {
    display: flex;
    flex-direction: row;
  }
  .game-info {
    margin-left: 20px;
  }
  ul {
    padding-left: 30px;
  }
</style>

<div class="game">
  <Board gameBoard={current.gameBoard} {handleClick} />
  <div class="game-info">
    <div class="status">{status}</div>
    <ul>
      {#each history as move, i}
        <li>
          <button on:click={e => jumpTo(i)}>
            {i ? 'Go to move #' + i : 'Go to game start'}
          </button>
        </li>
      {/each}
    </ul>
  </div>
</div>
