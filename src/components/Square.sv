<script>
  export let gameBoard;

  export let handleClick;
  export let id;

  $: value = gameBoard[id];
</script>

<style>
  .square {
    background: #fff;
    border: 1px solid #999;
    float: left;
    font-size: 24px;
    font-weight: bold;
    line-height: 34px;
    height: 34px;
    margin-right: -1px;
    margin-top: -1px;
    padding: 0;
    text-align: center;
    width: 34px;
  }
</style>

<button class="square" on:click={e => handleClick(id)}>
  {value ? value : ''}
</button>
